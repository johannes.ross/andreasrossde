export type PostContent = {
	image: string;
	content: string;
	imageAltText: string;
};

export type Position = 'left' | 'right';
