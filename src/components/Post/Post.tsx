import './Post.scss';
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import { Position, PostContent } from '../../types';

type PostProps = PostContent & {
	imagePosition: Position;
};

export default function Post(props: PostProps) {
	const imageTag = (
		<Popup
			trigger={
				<img
					src={'/assets/images/' + props.image}
					alt={props.imageAltText}
					className="PostImage"
				/>
			}
			modal
		>
			{(close: () => void) => (
				<div className="modal">
					<button className="close" onClick={close}>
						&times;
					</button>
					<img
						src={'/assets/images/' + props.image}
						alt={props.imageAltText}
						className="PopupImage"
					/>
				</div>
			)}
		</Popup>
	);

	return (
		<div className="SinglePost">
			{props.imagePosition === 'left' && imageTag}
			<div className="PostText">{props.content}</div>
			{props.imagePosition === 'right' && imageTag}
			imageTag
		</div>
	);
}
