import { Link } from 'react-router-dom';
import { SITELINKS, sRahmALinkClass } from '../../constants';
import './Menu.scss';

export default function Menu() {
	let aLinkCssClass = '';
	if (window.screen.width > 500) {
		aLinkCssClass = sRahmALinkClass;
	}
	return (
		<header className="ross-menu srahm-header">
			<nav className="srahm-nav">
				<div className="navbartitle">Andreas Roß</div>
				<Link to={SITELINKS.Homepage} className={aLinkCssClass}>
					Startseite
				</Link>
				<Link to={SITELINKS.Podcast} className={aLinkCssClass}>
					Podcast: Hoffnungswort
				</Link>
				<Link to={SITELINKS.Worship} className={aLinkCssClass}>
					Gottesdienste
				</Link>
				<Link to={SITELINKS.VideoProjects} className={aLinkCssClass}>
					Video Projekte
				</Link>
			</nav>
		</header>
	);
}
