export enum SITELINKS {
	Homepage = '/',
	Podcast = '/podcast-hoffnungswort',
	Worship = '/gottesdienste',
	VideoProjects = '/video-projekte',
}
export const sRahmALinkClass = 'srahm-a-Link';
