import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Menu from '../../components/Menu';
import { SITELINKS } from '../../constants';
import Startpage from '../Startpage';

export default function Main() {
	return (
		<Router>
			<Menu />
			<Switch>
				<Route exact path={SITELINKS.Homepage} component={Startpage} />
			</Switch>
		</Router>
	);
}
