import Post from '../../components/Post';
import { Position, PostContent } from '../../types';
import './Startpage.scss';

export const contents: PostContent[] = [
	{
		content:
			'Bacon ipsum dolor amet alcatra meatball tenderloin tongue filet mignon swine. Ham hock short ribs corned beef tenderloin, tongue turkey boudin chicken ribeye. Swine leberkas meatloaf, ball tip buffalo salami flank beef shankle frankfurter ham hock spare ribs andouille. Meatloaf pork loin meatball drumstick jerky, ham pancetta andouille ground round filet mignon ribeye chicken fatback leberkas shoulder. Landjaeger buffalo short ribs drumstick swine capicola ribeye cupim venison turducken pork belly pig. Swine sirloin rump short loin tail chislic flank hamburger spare ribs. Jowl ball tip andouille ground round, sausage burgdoggen tenderloin flank leberkas kevin jerky pork filet mignon kielbasa tail. Jowl cupim strip steak salami shankle beef ribs chuck doner jerky ham turducken. Shank tri-tip corned beef, pork belly porchetta venison short ribs landjaeger shoulder pork loin. Bacon burgdoggen spare ribs, strip steak picanha capicola rump short loin tail shank salami sausage filet mignon pig. Buffalo frankfurter rump, shoulder beef pig biltong tail fatback. Leberkas doner pig frankfurter salami meatball pork jerky prosciutto. Kielbasa fatback cupim t-bone sausage salami ham beef ball tip bacon tail biltong filet mignon strip steak. Cow meatloaf short ribs leberkas, pork belly jowl tenderloin sirloin. Turkey capicola jerky, short ribs meatball chicken shoulder spare ribs pork chop rump. Swine tri-tip turkey, alcatra rump cupim pork belly picanha pig pancetta bresaola turducken ham fatback. Ham hock drumstick tenderloin, cupim kielbasa venison andouille corned beef. Strip steak meatloaf filet mignon sausage, t-bone tenderloin buffalo capicola salami turducken. Ham jerky ham hock picanha. Tri-tip andouille cow pancetta biltong swine flank burgdoggen meatball beef brisket jerky short ribs ham beef ribs. Pork loin jerky ground round prosciutto, shoulder flank jowl turducken biltong. Beef alcatra bresaola capicola meatball chuck pork belly andouille pastrami burgdoggen ground round chislic biltong chicken turducken. Kevin hamburger sausage ham. Tongue ham hock shank venison landjaeger beef ribs. T-bone brisket turducken chicken sausage salami hamburger tri-tip swine. Rump drumstick pancetta, beef jerky kielbasa doner ball tip prosciutto filet mignon.',
		image: 'pfarrer-ross-aus-hildrizhausen-koordiniert-hilfeleistungen.jpg',
		imageAltText:
			'Pfarrer Roß aus Hildrizhausen koordiniert Hilfeleistungen',
	},
	{
		content:
			'Bacon ipsum dolor amet alcatra meatball tender tongue turkey boudin chicken ribeye. Swine leberkas meatloaf, ball tip buffalo salami flank beef shankle frankfurter ham hock spare ribs andouille. Meatloaf pork loin meatball drumstick jerky, ham pancetta andouille ground round filet mignon ribeye chicken fatback leberkas shoulder. Landjaeger buffalo short ribs drumstick swine capicola ribeye cupim venison turducken pork belly pig. Swine sirloin rump short loin tail chislic flank hamburger spare ribs. Jowl ball tip andouille ground round, sausage burgdoggen tenderloin flank leberkas kevin jerky pork filet mignon kielbasa tail. Jowl cupim strip steak salami shankle beef ribs chuck doner jerky ham turducken. Shank tri-tip corned beef, pork belly porchetta venison short ribs landjaeger shoulder pork loin. Bacon burgdoggen spare ribs, strip steak picanha capicola rump short loin tail shank salami sausage filet mignon pig. Buffalo frankfurter rump, shoulder beef pig biltong tail fatback. Leberkas doner pig frankfurter salami meatball pork jerky prosciutto. Kielbasa fatback cupim t-bone sausage salami ham beef ball tip bacon tail biltong filet mignon strip steak. Cow meatloaf short ribs leberkas, pork belly jowl tenderloin sirloin. Turkey capicola jerky, short ribs meatball chicken shoulder spare ribs pork chop rump. Swine tri-tip turkey, alcatra rump cupim pork belly picanha pig pancetta bresaola turducken ham fatback. Ham hock drumstick tenderloin, cupim kielbasa venison andouille corned beef. Strip steak meatloaf filet mignon sausage, t-bone tenderloin buffalo capicola salami turducken. Ham jerky ham hock picanha. Tri-tip andouille cow pancetta biltong swine flank burgdoggen meatball beef brisket jerky short ribs ham beef ribs. Pork loin jerky ground round prosciutto, shoulder flank jowl turducken biltong. Beef alcatra bresaola capicola meatball chuck pork belly andouille pastrami burgdoggen ground round chislic biltong chicken turducken. Kevin hamburger sausage ham. Tongue ham hock shank venison landjaeger beef ribs. T-bone brisket turducken chicken sausage salami hamburger tri-tip swine. Rump drumstick pancetta, beef jerky kielbasa doner ball tip prosciutto filet mignon.',
		image: 'pfarrer-ross-aus-hildrizhausen-koordiniert-hilfeleistungen.jpg',
		imageAltText:
			'Pfarrer Roß aus Hildrizhausen koordiniert Hilfeleistungen',
	},
];

export default function Startpage() {
	let leftOrRight: Position = 'right';
	const posts = contents.map((post) => {
		leftOrRight = leftOrRight === 'left' ? 'right' : 'left';
		return (
			<Post
				key={post.content}
				content={post.content}
				image={post.image}
				imageAltText={post.imageAltText}
				imagePosition={leftOrRight}
			/>
		);
	});

	return <div className="StartpageContainer">{posts}</div>;
}
